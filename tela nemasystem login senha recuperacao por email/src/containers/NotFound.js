import React from "react";
import "./NotFound.css";

export default function NotFound() {
  return (
    <div className="NotFound">
      <h3>Desculpe, pagina não encontrada!</h3>
    </div>
  );
}
